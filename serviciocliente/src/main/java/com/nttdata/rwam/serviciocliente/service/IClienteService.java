package com.nttdata.rwam.serviciocliente.service;



import com.nttdata.rwam.serviciocliente.entity.Cliente;

import java.util.List;

public interface IClienteService {
    List<Cliente> buscarUsuarios();

    List<Cliente> buscarUsuariosPorName(String nombre);

    Cliente crearUsuario(Cliente Cliente);

    Cliente actualizarUsuario(Long id, Cliente Cliente);

    String eliminarUsuario(Long id);

}
