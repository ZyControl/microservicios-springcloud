package com.nttdata.rwam.serviciocliente.repository;


import com.nttdata.rwam.serviciocliente.entity.Cliente;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
@Repository
public interface ClienteRepository extends JpaRepository<Cliente, Long> {

    List<Cliente> findByNombreContaining(String name);
}
