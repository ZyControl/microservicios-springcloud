package com.nttdata.rwam.serviciocliente.controller;

import com.nttdata.rwam.serviciocliente.entity.Cliente;
import com.nttdata.rwam.serviciocliente.service.IClienteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
//@RequestMapping("/usuario")
public class ClienteController {
//    @Value("${ruta.servicio.externo1}")
//    private String propiedad1;
//
//    @Value("${ruta.servicio.externo2}")
//    private String propiedad2;

    @Autowired
    private IClienteService iClienteService;

    @GetMapping("/listar")
    List<Cliente> listarUsuario() {
        return iClienteService.buscarUsuarios();
    }

    @GetMapping("/buscar/nombre")
    List<Cliente> buscarUsuariosPorName(@RequestParam("nombre") String name) {
        return iClienteService.buscarUsuariosPorName(name);
    }


    @PostMapping("/crear")
    Cliente crear(@RequestBody Cliente usuario) {
        return iClienteService.crearUsuario(usuario);
    }

    @PutMapping("/actualizar/{id}")
    Cliente actualizar(@RequestHeader HttpHeaders headers, @PathVariable("id") Long id, @RequestBody Cliente Cliente) {
        System.out.println("Cabecera: "+ headers.toString());
        return iClienteService.actualizarUsuario(id, Cliente);
    }

    @DeleteMapping("/eliminar/{id}")
    String eliminar(@PathVariable("id") Long id) {
        return iClienteService.eliminarUsuario(id);
    }


}
