package com.nttdata.rwam.serviciocliente.service;

import com.nttdata.rwam.serviciocliente.entity.Cliente;
import com.nttdata.rwam.serviciocliente.repository.ClienteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ClienteServiceImpl implements IClienteService {

    @Autowired
    private ClienteRepository iClienteRepository;

    @Override
    public List<Cliente> buscarUsuarios() {

        return iClienteRepository.findAll();
    }


    @Override
    public List<Cliente> buscarUsuariosPorName(String name) {
        return iClienteRepository.findByNombreContaining(name);
    }

    @Override
    public Cliente crearUsuario(Cliente usuario) {

        return iClienteRepository.save(usuario);
    }

    @Override
    public Cliente actualizarUsuario(Long id, Cliente usuario) {
        usuario.setId(id);
        return iClienteRepository.save(usuario);
    }


    @Override
    public String eliminarUsuario(Long id) {
        try {
            iClienteRepository.deleteById(id);
            return "Eliminacion Correcta";
        } catch (Exception ex) {
            return "Error durante la eliminacion";
        }
    }
}
