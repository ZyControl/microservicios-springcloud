package com.nttdata.rwam.serviciobanco.controller;

import com.nttdata.rwam.serviciobanco.entity.Banco;
import com.nttdata.rwam.serviciobanco.service.IBancoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
//@RequestMapping("/usuario")
public class BancoController {
//    @Value("${ruta.servicio.externo1}")
//    private String propiedad1;
//
//    @Value("${ruta.servicio.externo2}")
//    private String propiedad2;

    @Autowired
    private IBancoService iBancoService;

    @GetMapping("/listar")
    List<Banco> listarUsuario() {
        return iBancoService.buscarUsuarios();
    }

    @GetMapping("/buscar/nombre")
    List<Banco> buscarUsuariosPorName(@RequestParam("nombre") String name) {
        return iBancoService.buscarUsuariosPorName(name);
    }


    @PostMapping("/crear")
    Banco crear(@RequestBody Banco usuario) {
        return iBancoService.crearUsuario(usuario);
    }

    @PutMapping("/actualizar/{id}")
    Banco actualizar(@RequestHeader HttpHeaders headers, @PathVariable("id") Long id, @RequestBody Banco banco) {
        System.out.println("Cabecera: "+ headers.toString());
        return iBancoService.actualizarUsuario(id, banco);
    }

    @DeleteMapping("/eliminar/{id}")
    String eliminar(@PathVariable("id") Long id) {
        return iBancoService.eliminarUsuario(id);
    }


}
