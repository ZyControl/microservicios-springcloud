package com.nttdata.rwam.serviciobanco.service;



import com.nttdata.rwam.serviciobanco.entity.Banco;

import java.util.List;

public interface IBancoService {
    List<Banco> buscarUsuarios();

    List<Banco> buscarUsuariosPorName(String nombre);

    Banco crearUsuario(Banco banco);

    Banco actualizarUsuario(Long id, Banco banco);

    String eliminarUsuario(Long id);

}
