package com.nttdata.rwam.serviciobanco;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ServiciobancoApplication {

	public static void main(String[] args) {
		SpringApplication.run(ServiciobancoApplication.class, args);
	}

}
