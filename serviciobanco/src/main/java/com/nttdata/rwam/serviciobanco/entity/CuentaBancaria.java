package com.nttdata.rwam.serviciobanco.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.*;

import javax.persistence.*;
import java.util.Date;

@Entity
@Getter // genera todos los getters
@Setter // genera todos los setters
@AllArgsConstructor// genera constructor con todos sus atributos
@NoArgsConstructor// genera constructor vacio
@JsonIgnoreProperties(value = {"hibernateLazyInitializer", "handler"},ignoreUnknown = true)
public class CuentaBancaria {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String nroCuenta;

    private String tipoCuenta;

    private Boolean active;

    @ManyToOne()
    @JoinColumn(name = "id_banco", referencedColumnName = "id", nullable = true)
    @JsonIgnoreProperties("cuentaBancarias")
    private Banco banco;

    @ManyToOne()
    @JoinColumn(name = "id_cliente", referencedColumnName = "id", nullable = true)
    @JsonIgnoreProperties("cuentaBancarias")
    private Cliente cliente;

}
