package com.nttdata.rwam.serviciobanco.service;



import com.nttdata.rwam.serviciobanco.entity.Banco;
import com.nttdata.rwam.serviciobanco.repository.BancoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class BancoServiceImpl implements IBancoService {

    @Autowired
    private BancoRepository iBancoRepository;

    @Override
    public List<Banco> buscarUsuarios() {

        return iBancoRepository.findAll();
    }


    @Override
    public List<Banco> buscarUsuariosPorName(String name) {
        return iBancoRepository.findByNombreContaining(name);
    }

    @Override
    public Banco crearUsuario(Banco usuario) {

        return iBancoRepository.save(usuario);
    }

    @Override
    public Banco actualizarUsuario(Long id, Banco usuario) {
        usuario.setId(id);
        return iBancoRepository.save(usuario);
    }


    @Override
    public String eliminarUsuario(Long id) {
        try {
            iBancoRepository.deleteById(id);
            return "Eliminacion Correcta";
        } catch (Exception ex) {
            return "Error durante la eliminacion";
        }
    }
}
