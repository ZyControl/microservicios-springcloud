package com.nttdata.rwam.serviciousuario.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Getter // genera todos los getters
@Setter // genera todos los setters
@AllArgsConstructor// genera constructor con todos sus atributos
@NoArgsConstructor// genera constructor vacio
@JsonIgnoreProperties(value = {"hibernateLazyInitializer", "handler"},ignoreUnknown = true)
public class Banco {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String nombre;

    private String direccion;

    private Boolean active;

    @ManyToOne(fetch = FetchType.LAZY)
    @JsonIgnoreProperties("cliente")
    @JoinColumn(name = "cuenta_bancaria", referencedColumnName = "id", nullable = true)
    private CuentaBancaria cuentaBancarias;
}
