package com.nttdata.rwam.serviciousuario.repository;



import com.nttdata.rwam.serviciousuario.entity.Usuario;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
@Repository
public interface UsuarioRepository extends JpaRepository<Usuario, Long> {

   // List<Usuario> findByNombreContaining(String name);
}
