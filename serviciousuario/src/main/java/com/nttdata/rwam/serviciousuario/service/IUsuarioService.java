package com.nttdata.rwam.serviciousuario.service;



import com.nttdata.rwam.serviciousuario.entity.Usuario;

import java.util.List;

public interface IUsuarioService {
    List<Usuario> buscarUsuarios();

    List<Usuario> buscarUsuariosPorName(String nombre);

    Usuario crearUsuario(Usuario Usuario);

    Usuario actualizarUsuario(Long id, Usuario Usuario);

    String eliminarUsuario(Long id);

}
