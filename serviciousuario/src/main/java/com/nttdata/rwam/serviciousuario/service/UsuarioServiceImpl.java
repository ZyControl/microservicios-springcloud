package com.nttdata.rwam.serviciousuario.service;

import com.nttdata.rwam.serviciousuario.entity.Usuario;
import com.nttdata.rwam.serviciousuario.repository.UsuarioRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UsuarioServiceImpl implements IUsuarioService {

    @Autowired
    private UsuarioRepository iUsuarioRepository;

    @Override
    public List<Usuario> buscarUsuarios() {

        return iUsuarioRepository.findAll();
    }


    @Override
    public List<Usuario> buscarUsuariosPorName(String name) {
        return null;
    }

    @Override
    public Usuario crearUsuario(Usuario usuario) {

        return iUsuarioRepository.save(usuario);
    }

    @Override
    public Usuario actualizarUsuario(Long id, Usuario usuario) {
        usuario.setId(id);
        return iUsuarioRepository.save(usuario);
    }


    @Override
    public String eliminarUsuario(Long id) {
        try {
            iUsuarioRepository.deleteById(id);
            return "Eliminacion Correcta";
        } catch (Exception ex) {
            return "Error durante la eliminacion";
        }
    }
}
