package com.nttdata.rwam.serviciousuario.controller;

import com.nttdata.rwam.serviciousuario.entity.Usuario;
import com.nttdata.rwam.serviciousuario.service.IUsuarioService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
//@RequestMapping("/usuario")
public class UsuarioController {
//    @Value("${ruta.servicio.externo1}")
//    private String propiedad1;
//
//    @Value("${ruta.servicio.externo2}")
//    private String propiedad2;

    @Autowired
    private IUsuarioService iUsuarioService;

    @GetMapping("/listar")
    List<Usuario> listarUsuario() {
        return iUsuarioService.buscarUsuarios();
    }

    @GetMapping("/buscar/nombre")
    List<Usuario> buscarUsuariosPorName(@RequestParam("nombre") String name) {
        return iUsuarioService.buscarUsuariosPorName(name);
    }


    @PostMapping("/crear")
    Usuario crear(@RequestBody Usuario usuario) {
        return iUsuarioService.crearUsuario(usuario);
    }

    @PutMapping("/actualizar/{id}")
    Usuario actualizar(@RequestHeader HttpHeaders headers, @PathVariable("id") Long id, @RequestBody Usuario Usuario) {
        System.out.println("Cabecera: "+ headers.toString());
        return iUsuarioService.actualizarUsuario(id, Usuario);
    }

    @DeleteMapping("/eliminar/{id}")
    String eliminar(@PathVariable("id") Long id) {
        return iUsuarioService.eliminarUsuario(id);
    }


}
