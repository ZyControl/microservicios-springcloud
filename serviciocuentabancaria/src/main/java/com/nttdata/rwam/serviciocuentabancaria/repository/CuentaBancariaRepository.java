package com.nttdata.rwam.serviciocuentabancaria.repository;


import com.nttdata.rwam.serviciocuentabancaria.entity.CuentaBancaria;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
@Repository
public interface CuentaBancariaRepository extends JpaRepository<CuentaBancaria, Long> {

   // List<CuentaBancaria> findByNombreContaining(String name);
}
