package com.nttdata.rwam.serviciocuentabancaria.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Getter // genera todos los getters
@Setter // genera todos los setters
@AllArgsConstructor// genera constructor con todos sus atributos
@NoArgsConstructor// genera constructor vacio
@JsonIgnoreProperties(value = {"hibernateLazyInitializer", "handler"},ignoreUnknown = true)
public class Usuario {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String usuario;

    private String clave;

    private Boolean active;

    @OneToOne
    @JoinColumn(name = "empleado_id",referencedColumnName = "id", nullable = true)
    @JsonIgnoreProperties("usuario")
    private Cliente cliente;

}
