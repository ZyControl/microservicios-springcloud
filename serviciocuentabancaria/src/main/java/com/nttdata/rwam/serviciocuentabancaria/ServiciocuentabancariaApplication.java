package com.nttdata.rwam.serviciocuentabancaria;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ServiciocuentabancariaApplication {

	public static void main(String[] args) {
		SpringApplication.run(ServiciocuentabancariaApplication.class, args);
	}

}
