package com.nttdata.rwam.serviciocuentabancaria.controller;

import com.nttdata.rwam.serviciocuentabancaria.entity.CuentaBancaria;
import com.nttdata.rwam.serviciocuentabancaria.service.ICuentaBancariaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
//@RequestMapping("/usuario")
public class CuentaBancariaController {
//    @Value("${ruta.servicio.externo1}")
//    private String propiedad1;
//
//    @Value("${ruta.servicio.externo2}")
//    private String propiedad2;

    @Autowired
    private ICuentaBancariaService iCuentaBancariaService;

    @GetMapping("/listar")
    List<CuentaBancaria> listarUsuario() {
        return iCuentaBancariaService.buscarUsuarios();
    }

    @GetMapping("/buscar/nombre")
    List<CuentaBancaria> buscarUsuariosPorName(@RequestParam("nombre") String name) {
        return iCuentaBancariaService.buscarUsuariosPorName(name);
    }


    @PostMapping("/crear")
    CuentaBancaria crear(@RequestBody CuentaBancaria usuario) {
        return iCuentaBancariaService.crearUsuario(usuario);
    }

    @PutMapping("/actualizar/{id}")
    CuentaBancaria actualizar(@RequestHeader HttpHeaders headers, @PathVariable("id") Long id, @RequestBody CuentaBancaria CuentaBancaria) {
        System.out.println("Cabecera: "+ headers.toString());
        return iCuentaBancariaService.actualizarUsuario(id, CuentaBancaria);
    }

    @DeleteMapping("/eliminar/{id}")
    String eliminar(@PathVariable("id") Long id) {
        return iCuentaBancariaService.eliminarUsuario(id);
    }


}
