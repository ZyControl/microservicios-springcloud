package com.nttdata.rwam.serviciocuentabancaria.service;


import com.nttdata.rwam.serviciocuentabancaria.entity.CuentaBancaria;

import java.util.List;

public interface ICuentaBancariaService {
    List<CuentaBancaria> buscarUsuarios();

    List<CuentaBancaria> buscarUsuariosPorName(String nombre);

    CuentaBancaria crearUsuario(CuentaBancaria CuentaBancaria);

    CuentaBancaria actualizarUsuario(Long id, CuentaBancaria CuentaBancaria);

    String eliminarUsuario(Long id);

}
