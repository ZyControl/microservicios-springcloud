package com.nttdata.rwam.serviciocuentabancaria.service;

import com.nttdata.rwam.serviciocuentabancaria.entity.CuentaBancaria;
import com.nttdata.rwam.serviciocuentabancaria.repository.CuentaBancariaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CuentaBancariaServiceImpl implements ICuentaBancariaService {

    @Autowired
    private CuentaBancariaRepository iCuentaBancariaRepository;

    @Override
    public List<CuentaBancaria> buscarUsuarios() {

        return iCuentaBancariaRepository.findAll();
    }


    @Override
    public List<CuentaBancaria> buscarUsuariosPorName(String name) {
        return null;
    }

    @Override
    public CuentaBancaria crearUsuario(CuentaBancaria usuario) {

        return iCuentaBancariaRepository.save(usuario);
    }

    @Override
    public CuentaBancaria actualizarUsuario(Long id, CuentaBancaria usuario) {
        usuario.setId(id);
        return iCuentaBancariaRepository.save(usuario);
    }


    @Override
    public String eliminarUsuario(Long id) {
        try {
            iCuentaBancariaRepository.deleteById(id);
            return "Eliminacion Correcta";
        } catch (Exception ex) {
            return "Error durante la eliminacion";
        }
    }
}
